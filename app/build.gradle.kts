plugins {
    id("com.android.application")
    kotlin("android")
    kotlin("kapt")
    id("kotlin-android-extensions")
    id("com.google.gms.google-services")
}

android {
    compileSdkVersion(ConfigData.compileSdk)
    buildToolsVersion(ConfigData.buildTools)
    defaultConfig {
        applicationId = "com.paypay.android_personality_test"
        minSdkVersion(ConfigData.minSdk)
        targetSdkVersion(ConfigData.targetSdk)
        versionCode = ConfigData.versionCode
        versionName = ConfigData.versionName
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    testOptions {
        unitTests.isReturnDefaultValues = true
    }
}

dependencies {
    implementation(AppDependencies.Deps.Kotlin.kotlin)
    implementation(AppDependencies.Deps.Kotlin.kotlinCoreKtx)
    implementation(AppDependencies.Deps.Kotlin.kotlinLifecycleKtx)

    implementation(AppDependencies.Deps.AndroidX.appCompat)
    implementation(AppDependencies.Deps.AndroidX.recyclerView)
    implementation(AppDependencies.Deps.AndroidX.cardView)
    implementation(AppDependencies.Deps.Others.materialDesign)

    implementation(AppDependencies.Deps.Others.timber)
    implementation(AppDependencies.Deps.Others.constraintLayout)
    implementation(AppDependencies.Deps.Kotlin.kotlinFragmentKtx)
    implementation(AppDependencies.Deps.Others.prefsDataStore)

    //Net-working
    implementation(AppDependencies.Deps.Networking.retrofitRuntime)
    implementation(AppDependencies.Deps.Networking.retrofitGson)
    implementation(AppDependencies.Deps.Networking.okhttp)
    implementation(AppDependencies.Deps.Networking.okhttpLoggingInterceptor)

    //Dagger
    implementation(AppDependencies.Deps.Dagger.daggerRuntime)
    implementation(AppDependencies.Deps.Dagger.daggerAndroid)
    implementation(AppDependencies.Deps.Dagger.daggerAndroidSupport)
    kapt(AppDependencies.Deps.Dagger.daggerCompiler)
    kapt(AppDependencies.Deps.Dagger.daggerProcessor)

    //
    implementation("com.google.firebase:firebase-database-ktx:20.0.1")
    implementation("com.google.firebase:firebase-storage-ktx:20.0.0")
    implementation("com.google.firebase:firebase-auth-ktx:21.0.1")

    testImplementation(AppDependencies.Deps.Testing.junit)
    testImplementation(AppDependencies.Deps.Testing.truth)
    testImplementation(AppDependencies.Deps.Testing.kotlinCoroutinesTest)
    testImplementation(AppDependencies.Deps.Testing.mockitoCore)
    testImplementation(AppDependencies.Deps.Testing.mockWebServer)
    androidTestImplementation(AppDependencies.Deps.Testing.atslExtJunit)
    androidTestImplementation(AppDependencies.Deps.Testing.espressoCore)
    testImplementation(AppDependencies.Deps.Testing.androidXcoreTesting)
}