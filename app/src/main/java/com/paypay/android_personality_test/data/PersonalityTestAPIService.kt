package com.paypay.android_personality_test.data

import com.paypay.android_personality_test.domain.models.CategoriesListResponse
import retrofit2.http.GET

interface PersonalityTestAPIService {
    @GET("https://firebasestorage.googleapis.com/v0/b/personality-test-8f8f1.appspot.com/o/personality_test.json?alt=media&token=57f7059b-310b-4467-9a14-28426fc0a830")
    suspend fun fetchCategoriesList(): CategoriesListResponse?

}