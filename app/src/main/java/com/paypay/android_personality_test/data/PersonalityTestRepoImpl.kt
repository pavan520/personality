package com.paypay.android_personality_test.data

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.paypay.android_personality_test.domain.PersonalityTestRepo
import com.paypay.android_personality_test.domain.models.CategoriesListResponse
import com.paypay.android_personality_test.domain.models.Either
import com.paypay.android_personality_test.domain.models.SelectionModel
import com.paypay.android_personality_test.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class PersonalityTestRepoImpl @Inject constructor(
    private val apiService: PersonalityTestAPIService,
    private val firebaseAuth: FirebaseAuth,
    private val firebaseDBReference: DatabaseReference
) : PersonalityTestRepo {

    var currentUser: FirebaseUser? = null

    override suspend fun fetchCategories(): Either<CategoriesListResponse?> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val response = apiService.fetchCategoriesList()
                if (response?.errorInfo != null) {
                    val error = response?.errorInfo?.info ?: "Something went wrong"
                    Either.Error(Exception(error))
                } else {
                    Either.Success(response)
                }
            } catch (e: Exception) {
                Timber.e(e)
                Either.Error(e)
            }
        }

    override suspend fun createAnonymousUser() {
        currentUser = firebaseAuth.currentUser
        if (currentUser == null) {
            firebaseAuth.signInAnonymously()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        currentUser = firebaseAuth.currentUser
                    }
                }
        }
    }


    override suspend fun updateDataToFirebase(data: CategoriesListResponse) {
        val map = HashMap<String, Any>()
        data.categories?.forEach { categoryName ->
            val list = ArrayList<SelectionModel>()
            data.questions?.filter {
                it.questionCategory == categoryName
            }?.forEachIndexed { index, question ->
                if((question.questionType?.type).equals(Constants.QUESTION_TYPE_CONDITIONAL,ignoreCase = true) &&
                    !question.questionType?.condition?.innerQuestion?.selectedOptions.isNullOrBlank()) {
                   val innerData = question.questionType?.condition?.innerQuestion
                   val innerModelData = SelectionModel(
                       innerData?.questionCategory,
                       innerData?.questionData,
                       innerData?.selectedOptions
                    )
                    list.add(
                        SelectionModel(
                            question.questionCategory,
                            question.questionData,
                            question.selectedOptions,
                            innerModelData
                        )
                    )
                } else if (!question.selectedOptions.isNullOrBlank()) {
                    list.add(
                        SelectionModel(
                            question.questionCategory,
                            question.questionData,
                            question.selectedOptions
                        )
                    )
                }
            }
            if (!list.isNullOrEmpty()) {
                map[categoryName] = list
            }
        }

        firebaseDBReference.child(Constants.USERS_INFO_TABLE)
            .child(currentUser?.uid ?: UUID.randomUUID().toString())
            .updateChildren(map)
            .addOnSuccessListener {
                Either.Success("")
            }
            .addOnFailureListener {
                Either.Error(it)
            }
    }

}