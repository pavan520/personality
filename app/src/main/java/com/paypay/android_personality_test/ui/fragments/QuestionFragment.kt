package com.paypay.android_personality_test.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.paypay.android_personality_test.R
import com.paypay.android_personality_test.domain.models.Question
import com.paypay.android_personality_test.domain.models.QuestionType
import com.paypay.android_personality_test.ui.adapters.OptionsAdapter
import com.paypay.android_personality_test.ui.viewmodels.PersonalityTestViewModel
import com.paypay.android_personality_test.utils.Constants
import com.paypay.android_personality_test.utils.showAgeRangeDialog
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_question.*

class QuestionFragment : DaggerFragment()  {
    private val personalityTestViewModel: PersonalityTestViewModel by viewModels({
        requireActivity()
    })

    private val optionsAdapter by lazy { OptionsAdapter(::onOptionSelected) }

    private fun onOptionSelected(option: String) {
        personalityTestViewModel.updateTestProgress(option)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_question, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        setUpViews()
    }

    private fun setUpViews() {
        rv_options?.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = optionsAdapter
        }
    }

    fun observeViewModel(){

        personalityTestViewModel.apply {
            testProgressLD?.observe(viewLifecycleOwner,  {
                if(!it.currentCategory.isNullOrBlank()) {
                    personalityTestViewModel.getDataForTestProgress()
                }else{
                    activity?.onBackPressed()
                }
            })
            responseQuestionDataLD?.observe(viewLifecycleOwner,  {
                it?.let {
                    if((it.questionType?.type).equals(Constants.QUESTION_TYPE_RANGE,ignoreCase = true)) {
                         showAgeSelectionPopUp(it)
                    }else{
                        updateUIWithCurrentQuestion(it)
                    }
                }
            })
        }
    }

    private fun showAgeSelectionPopUp(question: Question) {
        requireContext().showAgeRangeDialog(question,::onOptionSelected)
    }

    private fun updateUIWithCurrentQuestion(question: Question?) {
      question?.let {
          if(txt_category_name?.text != it.questionCategory){
              txt_category_name?.text = it.questionCategory
          }
          txt_question?.text = it.questionData
          updateAdapterData(it.questionType,it.selectedOptions?:"")
      }
    }

    private fun updateAdapterData(questionType: QuestionType?, selectedOptions: String) {
        if(questionType !=null){
            optionsAdapter.setData(questionType,selectedOptions)
        }
    }


}