package com.paypay.android_personality_test.ui.adapters.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_options.view.*

class OptionsViewHolder(itemView: View,
                        private val onOptionSelected: (selected: String) -> Unit) : RecyclerView.ViewHolder(itemView) {

    fun setData(option: String, isSelectedOptionList: Boolean) {
        itemView?.apply {
            txt_option?.text = option
            setOnClickListener {
                onOptionSelected.invoke(option)
            }
        }
    }
}