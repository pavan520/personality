package com.paypay.android_personality_test.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.paypay.android_personality_test.R
import com.paypay.android_personality_test.domain.models.QuestionType
import com.paypay.android_personality_test.ui.adapters.viewholders.OptionsViewHolder

class OptionsAdapter(private val onOptionSelected: (selected: String) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val optionsList = arrayListOf<String>()
    private var selectedOptionList :String ?=null

    fun setData(value: QuestionType, selectedOptions: String) {
        optionsList.clear()
        value.optionsList?.let {
            optionsList.addAll(it)
            selectedOptionList = selectedOptions
            notifyDataSetChanged()
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return OptionsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_options,
                parent,
                false
            ), onOptionSelected
        )
    }

    override fun getItemCount(): Int = optionsList.size


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as OptionsViewHolder).setData(
            optionsList[position],
            (selectedOptionList?:"").contains(optionsList[position])
        )
    }


}