package com.paypay.android_personality_test.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import com.paypay.android_personality_test.R
import com.paypay.android_personality_test.ui.viewmodels.PersonalityTestViewModel
import com.paypay.android_personality_test.utils.isNetworkAvailable
import com.paypay.android_personality_test.utils.remove
import com.paypay.android_personality_test.utils.show
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.layout_zero_case.*

class HomeFragment : DaggerFragment() {

    private val personalityTestViewModel: PersonalityTestViewModel by viewModels({
        requireActivity()
    })

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_home, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        setUpViews()
    }

    private fun observeViewModel() {
        personalityTestViewModel.apply {
            isLoading.observe(viewLifecycleOwner, {
                it?.let {
                    showOrHideLoading(it)
                }
            })
            error.observe(viewLifecycleOwner, {
               if(!it.isNullOrBlank()){
                    showErrorView(it)
                }
            })
            responseDataLD.observe(viewLifecycleOwner, {
                it?.let {
                    if(!it.categories.isNullOrEmpty() || !it.questions.isNullOrEmpty()) {
                        toggleStartVisibility(true)
                    }else{
                        showEmptyView()
                    }
                }
            })
        }
    }

    private fun showEmptyView() {
        tv_zero_case?.text = resources.getString(R.string.no_data)
        btn_retry?.remove()
        empty_layout?.show()
    }

    private fun showErrorView(msg: String) {
        var errorMSg = ""
        if (!requireContext().isNetworkAvailable()) {
            errorMSg = resources.getString(R.string.network_error)
        } else if (!msg.isNullOrBlank()) {
            errorMSg = msg
        } else {
            errorMSg = resources.getString(R.string.something_wrong)
        }
        toggleStartVisibility(false)
        tv_zero_case?.text = errorMSg
        btn_retry?.show()
        empty_layout?.show()
        btn_retry?.setOnClickListener {
            btn_retry?.remove()
            empty_layout?.remove()
            loadData()
        }
    }

    private fun showOrHideLoading(status: Boolean) {
        if (status) {
            progress_circular?.show()
        } else {
            progress_circular?.remove()
        }
    }

    private fun toggleStartVisibility(status: Boolean) {
        if (status) {
            btn_start_test?.show()
        } else {
            btn_start_test?.remove()
        }
    }

    private fun loadData() {
        personalityTestViewModel.fetchCategoriesList()
    }

    private fun setUpViews() {
        initListeners()
    }

    private fun initListeners() {
        btn_start_test?.setOnClickListener {
            if (personalityTestViewModel.checkCategoriesAndQuestionData()) {
                personalityTestViewModel.setInitialData()
                addQuestionSelectionFragment()
            }
        }
    }

    private fun addQuestionSelectionFragment() {
        val fragmentTransaction: FragmentTransaction? =
            activity?.supportFragmentManager?.beginTransaction()
        fragmentTransaction?.replace(R.id.fragment_container_view, QuestionFragment())
        fragmentTransaction?.addToBackStack(QuestionFragment::class.simpleName)
        fragmentTransaction?.commit() // save the changes
    }
}