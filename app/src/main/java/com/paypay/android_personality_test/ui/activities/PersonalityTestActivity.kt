package com.paypay.android_personality_test.ui.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider
import com.paypay.android_personality_test.R
import com.paypay.android_personality_test.ui.viewmodels.PersonalityTestViewModel
import dagger.android.support.DaggerAppCompatActivity
import java.lang.ref.WeakReference
import javax.inject.Inject

class PersonalityTestActivity : DaggerAppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val personalityTestViewModel: PersonalityTestViewModel by viewModels {
        viewModelFactory
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personality_test)
        loadData()
    }

    private fun loadData() {
        personalityTestViewModel.checkAndCreateFirebaseUser()
        personalityTestViewModel.fetchCategoriesList()
    }

    companion object {
        fun startActivity(activityWeakReference: WeakReference<Activity>) {
            val intent = Intent(activityWeakReference.get(), PersonalityTestActivity::class.java)
            activityWeakReference.get()?.startActivity(intent)
        }
    }
}