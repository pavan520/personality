package com.paypay.android_personality_test.ui.activities

import android.os.Bundle
import com.paypay.android_personality_test.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.coroutines.*
import java.lang.ref.WeakReference


class SplashActivity : DaggerAppCompatActivity() {

    val activityScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setupViews()

    }

    fun setupViews() {
        activityScope.launch {
            delay(1500)
            navigateToHome()
        }
    }


    private fun navigateToHome() {
        PersonalityTestActivity.startActivity(WeakReference(this))
        finish()
    }

    override fun onPause() {
        super.onPause()
        activityScope.cancel()
    }
}