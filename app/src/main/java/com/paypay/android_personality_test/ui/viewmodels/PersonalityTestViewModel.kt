package com.paypay.android_personality_test.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.paypay.android_personality_test.domain.PersonalityTestRepo
import com.paypay.android_personality_test.domain.models.TestProgress
import com.paypay.android_personality_test.domain.models.CategoriesListResponse
import com.paypay.android_personality_test.domain.models.Either
import com.paypay.android_personality_test.domain.models.Question
import kotlinx.coroutines.launch
import javax.inject.Inject

class PersonalityTestViewModel @Inject constructor(
    private val dataRepo: PersonalityTestRepo) : ViewModel() {
    private val _isLoading by lazy { MutableLiveData<Boolean>() }
    val isLoading: LiveData<Boolean> by lazy { _isLoading }

    private val _error by lazy { MutableLiveData<String>() }
    val error: LiveData<String> by lazy { _error }

    private val _responseDataLD by lazy { MutableLiveData<CategoriesListResponse?>() }
    val responseDataLD by lazy { _responseDataLD }

    lateinit var testProgress: TestProgress

    private val _TestProgressLD by lazy { MutableLiveData<TestProgress>() }
    val testProgressLD by lazy { _TestProgressLD }

    private val _responseQuestionDataLD by lazy { MutableLiveData<Question?>() }
    val responseQuestionDataLD by lazy { _responseQuestionDataLD }

    fun fetchCategoriesList() {
        setLoadingState(true)
        viewModelScope.launch {
            dataRepo.fetchCategories()?.let {
                onResponseReceived(it)
            }
        }
    }

    private fun onResponseReceived(response: Either<CategoriesListResponse?>) {
        if (response is Either.Success) {
            _responseDataLD.postValue(response.data)
            _error.postValue("")
        } else if (response is Either.Error) {
            _error.postValue(response.exception?.message ?: "Something went wrong")
        }
        setLoadingState(false)
    }

    fun setLoadingState(state: Boolean) {
        _isLoading.postValue(state)
    }

    fun setInitialData() {
        if (checkCategoriesAndQuestionData()) {
            testProgress = TestProgress(_responseDataLD.value?.categories?.get(0), 0)
            _TestProgressLD.postValue(testProgress)
        }
    }

    fun updateTestProgress(option: String) {
        saveCurrentSelection(option)
        if (checkPredicateCondition(option)) {

        } else if (checkHasMoreCategoryQuestions()) {
            testProgress.currentQuestionPosition = testProgress.currentQuestionPosition?.plus(1)
            _TestProgressLD.postValue(testProgress)
        } else {
            val nextcategory = getNextCategory()
            testProgress.currentCategory = nextcategory
            testProgress.currentQuestionPosition = 0
            if (!nextcategory.isNullOrBlank()) {
                _TestProgressLD.postValue(testProgress)
            } else {
                endTheTest()
            }
        }
    }

    private fun checkPredicateCondition(option: String): Boolean {

        _responseQuestionDataLD.value?.let { question ->
            question.questionType?.condition?.let { condition ->
                condition.predicate?.let { predicate ->
                    if (!predicate.exactEquals.isNullOrEmpty() && predicate.exactEquals!!.contains(
                            option
                        )
                    ) {
                        condition.innerQuestion?.let {
                            _responseQuestionDataLD.postValue((it))
                            return true
                        }
                    }
                }
            }
        }

        return false
    }

    private fun saveCurrentSelection(option: String) {
        _responseQuestionDataLD.value?.let {
            it.selectedOptions = option
            updateToFireBase()
        }

    }

    private fun updateToFireBase() {
        _responseDataLD?.value?.let {
            viewModelScope.launch {
                dataRepo.updateDataToFirebase(it)?.let {
                }
            }
        }
    }

    private fun endTheTest() {
        testProgress = TestProgress(null, 1)
        _TestProgressLD.postValue(testProgress)

    }

    private fun getNextCategory(): String? {
        val nextCategoryIndex =
            _responseDataLD.value?.categories?.indexOf(testProgress.currentCategory)?.plus(1)
        return if (nextCategoryIndex ?: 0 < _responseDataLD.value?.categories?.size ?: 0) {
            _responseDataLD.value?.categories?.get(nextCategoryIndex ?: 0)
        } else {
            null
        }
    }

    private fun checkHasMoreCategoryQuestions(): Boolean {
        var hasMoreQuestions = false
        _responseDataLD.value?.questions?.filter {
            it.questionCategory == testProgress.currentCategory
        }?.apply {
            hasMoreQuestions = (size.minus(1)) > testProgress?.currentQuestionPosition ?: 0
        }
        return hasMoreQuestions
    }

    fun checkCategoriesAndQuestionData(): Boolean {
        return _responseDataLD.value?.categories?.size ?: 0 > 0
                && _responseDataLD.value?.categories?.size ?: 0 > 0
    }

    fun getDataForTestProgress() {
        _responseDataLD.value?.questions?.filter {
            it.questionCategory == testProgress.currentCategory
        }?.apply {
            if (size > testProgress?.currentQuestionPosition ?: 0) {
                _responseQuestionDataLD.postValue(get(testProgress?.currentQuestionPosition ?: 0))
            } else {
                endTheTest()
            }
        }
    }

    fun checkAndCreateFirebaseUser() {
        viewModelScope.launch {
            dataRepo.createAnonymousUser()
        }
    }


}