package com.paypay.android_personality_test.utils

import android.content.Context
import android.net.ConnectivityManager
import android.view.LayoutInflater
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.paypay.android_personality_test.R
import com.paypay.android_personality_test.domain.models.Question
import kotlinx.android.synthetic.main.dialog_range_selection.view.*

fun Context.isNetworkAvailable(): Boolean {
    val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return (activeNetworkInfo != null && activeNetworkInfo.isConnected)
}

fun Context.showAgeRangeDialog(question: Question, onOptionSelected: (selected: String) -> Unit) {
    val materialAlertDialogBuilder = MaterialAlertDialogBuilder(this)

    val  customAlertDialogView = LayoutInflater.from(this)
        .inflate(R.layout.dialog_range_selection, null, false)
    customAlertDialogView?.txt_dialog_question?.text = question?.questionData
    customAlertDialogView?.dialog_age_range_seekbar?.valueTo = Float.MAX_VALUE
    customAlertDialogView?.dialog_age_range_seekbar?.valueFrom = Float.MIN_VALUE

    val max = question.questionType?.range?.to
    val min = question.questionType?.range?.from
    customAlertDialogView?.dialog_age_range_seekbar?.valueTo = max?.toFloat()?:0.00f
    customAlertDialogView?.dialog_age_range_seekbar?.valueFrom = min?.toFloat()?:0.00f
    customAlertDialogView?.dialog_age_range_seekbar?.value = (max?.minus(min?:0))?.div(1)?.toFloat()?:1.00f

    val dialog = materialAlertDialogBuilder.setView(customAlertDialogView)
        .setCancelable(false)
        .show()
    customAlertDialogView?.btn_next?.setOnClickListener {
        onOptionSelected.invoke(customAlertDialogView.dialog_age_range_seekbar?.value.toString())
        dialog?.dismiss()
    }
}