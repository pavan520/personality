package com.paypay.android_personality_test.utils

object Constants {
    var USERS_INFO_TABLE = "users_info"
    var QUESTION_TYPE_RANGE = "number_range"
    var QUESTION_TYPE_CONDITIONAL = "single_choice_conditional"

}