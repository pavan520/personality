package com.paypay.android_personality_test.di.modules

import com.paypay.android_personality_test.ui.activities.PersonalityTestActivity
import com.paypay.android_personality_test.ui.activities.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {
    @ContributesAndroidInjector(
        modules = [
            FragmentBindingModule::class
        ]
    )
    abstract fun injectPersonalityTestActivity():PersonalityTestActivity

    @ContributesAndroidInjector
    abstract fun injectSplashActivity():SplashActivity

}