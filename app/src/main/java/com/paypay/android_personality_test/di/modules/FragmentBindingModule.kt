package com.paypay.android_personality_test.di.modules

import com.paypay.android_personality_test.ui.fragments.HomeFragment
import com.paypay.android_personality_test.ui.fragments.QuestionFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBindingModule {

    @ContributesAndroidInjector
    abstract fun injectHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    abstract fun injectQuestionFragment(): QuestionFragment

}