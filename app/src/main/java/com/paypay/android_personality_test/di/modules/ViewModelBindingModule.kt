package com.paypay.android_personality_test.di.modules

import androidx.lifecycle.ViewModel
import com.paypay.android_personality_test.ui.viewmodels.PersonalityTestViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Module
abstract class ViewModelBindingModule {
    @Binds
    @IntoMap
    @ViewModelKey(PersonalityTestViewModel::class)
    abstract fun bindPersonalityTestViewModel(fragmentViewModel: PersonalityTestViewModel): ViewModel

}

@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER
)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)