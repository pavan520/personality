package com.paypay.android_personality_test.di.modules

import android.content.Context
import androidx.annotation.Nullable
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStore
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.paypay.android_personality_test.BuildConfig
import com.paypay.android_personality_test.data.PersonalityTestAPIService
import com.paypay.android_personality_test.data.PersonalityTestRepoImpl
import com.paypay.android_personality_test.domain.PersonalityTestRepo
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
abstract class NetworkModule {
    @Binds
    abstract  fun bindingPersonalityTestDataRepo(personalityRepo: PersonalityTestRepoImpl): PersonalityTestRepo

    companion object {
        private val Context.dataStore by preferencesDataStore("app_cache")

        @Provides
        @Singleton
        fun bindDataStore(context: Context):DataStore<Preferences> = context.dataStore

        @Provides
         fun provideAuth(): FirebaseAuth {
            return  Firebase.auth
        }

        @Provides
        fun provideFirebaseDatabaseRef(): DatabaseReference {
            return  Firebase.database.reference
        }

      @Singleton
      @Provides
      fun provideGson(): Gson {
          return GsonBuilder()
              .setLenient()
              .create()
      }


        @Provides
        fun provideOkHttpClient(context: Context, gson: Gson): OkHttpClient {
            val okHttpBuilder = OkHttpClient.Builder()
            if (BuildConfig.DEBUG) {
                val loggingInterceptor = HttpLoggingInterceptor { message -> Timber.i(message) }
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                okHttpBuilder.addInterceptor(loggingInterceptor)
            }
            okHttpBuilder.readTimeout(30, TimeUnit.SECONDS)
            okHttpBuilder.connectTimeout(30, TimeUnit.SECONDS)
            okHttpBuilder.writeTimeout(30, TimeUnit.SECONDS)


            return okHttpBuilder.build()
        }

        @Provides
        @Singleton
        fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson): Retrofit {
            return Retrofit.Builder()
                .baseUrl("https://firebasestorage.googleapis.com/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        }

        @Provides
        @Singleton
        fun providePersonalityTestApiService(retrofit: Retrofit): PersonalityTestAPIService {
            return retrofit.create(PersonalityTestAPIService::class.java)
        }
  }

}