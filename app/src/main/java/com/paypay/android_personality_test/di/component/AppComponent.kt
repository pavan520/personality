package com.paypay.android_personality_test.di.component

import android.app.Application
import com.paypay.android_personality_test.PersonalityTestApplication
import com.paypay.android_personality_test.di.modules.AppModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class]
)
interface AppComponent: AndroidInjector<PersonalityTestApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(app: PersonalityTestApplication)

}