package com.paypay.android_personality_test.domain.models

import com.google.gson.annotations.SerializedName

data class CategoriesListResponse(
    @SerializedName("categories")
    var categories: List<String>?,
    @SerializedName("questions")
    var questions: List<Question>?,
    @SerializedName("error")
    var errorInfo: ErrorData? = null
)

data class ErrorData(
    @SerializedName("code")
    val code: Int?,
    @SerializedName("message")
    val info: String?
)

data class Question(
    @SerializedName("question")
    var questionData: String?,
    @SerializedName("category")
    var questionCategory: String?,
    @SerializedName("question_type")
    var questionType: QuestionType?,
    var selectedOptions: String?
)

data class QuestionType(
    @SerializedName("type")
    var type: String?,
    @SerializedName("options")
    var optionsList: List<String>?,
    @SerializedName("condition")
    var condition: Condition?,
    @SerializedName("range")
    var range: Range?
)

data class Condition(
    @SerializedName("predicate")
    var predicate: Predicate?,
    @SerializedName("if_positive")
    var innerQuestion: Question?
)

data class Range(
    @SerializedName("from")
    var from: Int?,
    @SerializedName("to")
    var to: Int?
)

data class Predicate(
    @SerializedName("exactEquals")
    var exactEquals: List<String>?
)