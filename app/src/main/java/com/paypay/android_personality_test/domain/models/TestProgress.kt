package com.paypay.android_personality_test.domain.models

data class TestProgress(
    var currentCategory:String?= null,
    var currentQuestionPosition:Int?= null,
)
