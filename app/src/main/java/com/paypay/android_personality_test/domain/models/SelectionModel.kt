package com.paypay.android_personality_test.domain.models

data class SelectionModel (
    var questionCategory:String?,
    var questionData:String?,
    var optionSelected:String?,
    var innerQuestion:SelectionModel?=null
)