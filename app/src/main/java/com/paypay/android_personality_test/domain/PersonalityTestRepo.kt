package com.paypay.android_personality_test.domain

import com.paypay.android_personality_test.domain.models.CategoriesListResponse
import com.paypay.android_personality_test.domain.models.Either

interface PersonalityTestRepo {
    suspend fun fetchCategories(): Either<CategoriesListResponse?>
    suspend fun createAnonymousUser()
    suspend fun updateDataToFirebase(data:CategoriesListResponse)

}