object Versions {

    const val gradlePlugin = "4.2.2"
    const val kotlinPlugin = "1.5.0"
    const val googleServices = "4.3.10"

    //Kotlin stuff
    const val kotlin = "1.5.0"
    const val coreKtx = "1.5.0"
    const val fragmentKtx ="1.2.0-rc01"
    const val lifecycleKtx = "2.3.1"
    const val coroutines = "1.5.0"

    const val appCompat = "1.3.1"
    const val recyclerView = "1.2.0"
    const val material = "1.4.0"
    const val support = "1.0.0"

    const val retrofit = "2.9.0"
    const val retrofitConverts = "2.6.1"
    const val okhttp = "4.9.0"

    const val timber = "4.7.1"
    const val constraintLayout = "2.0.4"
    const val prefsDataStore = "1.0.0-alpha08"

    const val dagger = "2.34"

    //Test Dependencies
    const val jUnit = "4.12"
    const val atslJunit = "1.1.2"
    const val espresso = "3.3.0"
    const val mockitoKotlin = "2.1.0"
    const val truth = "1.1.3"
    const val mockito ="2.28.2"
    const val rule = "1.1.1"
    const val runner = "1.1.1"
    const val archCore ="2.0.1"

}